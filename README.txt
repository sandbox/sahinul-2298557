Readme file for the Term ACL module for Drupal
---------------------------------------------

This module helps to manage access control to contents based on taxonomy terms 
referenced upon a content.
In situation where we need to allow view, create, edit and delete access to 
taxonomy terms for user roles for various contents. This module provides a 
second layer of acl to contents as per term reference upon user roles.


Installation:
  Installation is like with all normal drupal modules:
  extract the 'termacl' folder from the tar ball to the
  modules directory from your website (typically sites/all/modules).

Dependencies:
  taxonomy module is required. 

Configuration:
  There is no configuration required to operate this module. 
  All you have to do is while creating or editing a vocabulary just enable the 
  "Vocabulary ACL" field.
