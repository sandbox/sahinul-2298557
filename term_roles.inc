<?php

/**
 * @file
 * Term roles.
 */

/**
 * Term roles form.
 */
function termacl_term_roles_form($form, &$form_state, $edit = array()) {
  if (empty($edit)) {
    // Redirect to 404 error page.
    drupal_not_found();
  }
  drupal_set_title(t("Grant roles access to @name.", array("@name" => $edit->name)));

  $all_roles = user_roles();
  $all_perms = user_role_permissions($all_roles);

  $form["#term"] = $edit;
  $form["#all_roles"] = $all_roles;

  // Preparing Headers.
  $term_perms_ = termacl_perm_name($edit->name, "", TRUE);
  $header = array(
    'role' => array(
      'data' => t('Role'),
    ),
  );
  foreach ($term_perms_ as $p) {
    $header[$p] = array(
      'data' => ucwords($p),
    );
  }
  // End Preparing Headers.
  // Preparing Rows.
  $options = array();
  foreach ($all_roles as $rid => $role) {
    // Col 1.
    $options[$rid]["role"] = ucwords($role);

    $opt_chk = array(
      "#type" => "checkbox",
      "#title" => "",
      "#title_display" => "invisible",
      // Role id to be returned in submit.
      "#return_value" => $rid,
      "#default_value" => 0,
    );
    // Values for other cols.
    foreach ($term_perms_ as $p) {
      $default = intval(@$all_perms[$rid][termacl_perm_name($edit->name, $p)]);

      $opt_chk["#checked"] = ($default ? TRUE : FALSE);
      $opt_chk["#id"] = $p;
      $opt_chk["#name"] = $p . "[]";

      $options[$rid][$p]["data"] = $opt_chk;
    } // End for.
  }

  // Implements $form.
  $form['term_roles'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $options,
    '#empty' => t('No content available.'),
    '#sticky' => TRUE,
  );

  $form['btn_submit'] = array(
    '#type' => "submit",
    '#value' => t("Save permissions"),
  );
  return $form;
}

/**
 * Implements.
 */
function termacl_term_roles_form_submit($form, &$form_state) {

  $o_all = new stdClass();
  // A$posted = $form_state["input"];.
  $posted = $form_state["values"];

  $o_all->term = $form["#term"];
  // Get all permission names for this term.
  $o_all->allterm_permName = termacl_perm_name($o_all->term->name);
  // Get all roles.
  $all_roles = $form["#all_roles"];

  $permission = array();
  $permission = array_fill_keys(array_values($o_all->allterm_permName), 0);
  // Fillup with all rid and default all permissions.
  $o_all->return = array();
  $o_all->return = array_fill_keys(array_keys($all_roles), $permission);

  array_walk($posted, create_function('&$item, &$key, $o_all', '

    $tp = termacl_perm_name($o_all->term->name, $key);
    $k = in_array($tp, $o_all->allterm_permName);
    if($k !== FALSE){
      foreach($item as $rid){
        $o_all->return[$rid][$tp] = 1;
      } // End for.
    } // End if.
  '), $o_all);

  // Storing into db.
  array_walk($o_all->return, create_function('$item, $rid', 'user_role_change_permissions($rid, $item);return TRUE;'));
  drupal_set_message(t("The changes have been saved."));

}

/**
 * Implements.
 */
function termacl_roles_term_form($form, &$form_state, $edit = array()) {
  if (empty($edit)) {
    // Redirect to 404 error page.
    drupal_not_found();
  }
  drupal_set_title(t("Edit taxonomy access for role @name.", array("@name" => $edit->name)));

  $all_roles = user_roles();
  // All perms for a role $rid.
  $all_perms = user_role_permissions(array_fill_keys(array($edit->rid), $edit));
  // Fetch acl vocabulary.
  $all_acl_voc = termacl_get_aclvoc_tree();

  $form["#role"] = $edit;
  $form["#all_roles"] = $all_roles;
  $form["#all_perms"] = $all_perms;
  $form["#all_aclVoc"] = $all_acl_voc;
  $weight = 0;

  // Preparing Headers.
  $term_perms_ = termacl_perm_name($edit->name, "", TRUE);
  $header = array(
    'term' => array(
      'data' => t('Term'),
    ),
  );
  foreach ($term_perms_ as $p) {
    $header[$p] = array(
      'data' => ucwords($p),
    );
  }

  // All acl vocabulary.
  foreach ($all_acl_voc as $voc) {

    $form[$voc->vid]["voc"] = array(
      "#type" => "fieldset",
      "#title" => ($voc->name . " terms list"),
      '#collapsible' => TRUE,
      "#weight" => $weight++,
    );

    // Preparing Rows.
    $use = new stdClass();
    $use->options = array();
    $use->term_perms_ = $term_perms_;
    $use->all_perms = $all_perms;
    $use->edit = $edit;

    array_walk($voc->terms_tree, create_function('&$item, $key, $use', '
    $use->options[$item->tid]["term"] = t(ucwords($item->name));// Col 1.

    $opt_chk = array(
      "#type" => "checkbox",
      "#title" => "",
      "#title_display" => "invisible",
      // Term id to be returned in submit.
      "#return_value" => $item->tid,
      "#default_value" => 0,
    );
    // Values for other cols.
    foreach ($use->term_perms_ as $p) {
      $default = intval(@$use->all_perms[$use->edit->rid][termacl_perm_name($item->name, $p)]);

      $opt_chk["#checked"] = ($default ? TRUE : FALSE);
      $opt_chk["#id"] = $p;
      $opt_chk["#name"] = $p."[]";

      $use->options[$item->tid][$p]["data"] = $opt_chk;
    } // End for.
    '), $use);

    $form[$voc->vid]["voc"]["perm"] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $use->options,
      '#sticky' => TRUE,
      '#empty' => t('No content available.'),
      "#tree" => TRUE,
    );

  }

  $form['btn_submit'] = array(
    '#type' => "submit",
    '#value' => t("Save permissions"),
  );
  return $form;
}

/**
 * Implements.
 */
function termacl_roles_term_form_submit($form, &$form_state) {
  // A$posted = $form_state["input"];.
  $posted = $form_state["values"];

  $role = $form["#role"];
  $all_acl_voc = $form["#all_aclVoc"];
  // A$all_perms = $form["#all_perms"];.
  $use = new stdClass();
  $use->return = array();
  $use->role = $role;
  $use->posted = $posted;

  foreach ($all_acl_voc as $voc) {
    // Looping each term under $vid.
    array_walk($voc->terms_tree, create_function('&$term, $key, $use', '
    $all_termPerm = termacl_perm_name($term->name, "", TRUE);
    foreach ($all_termPerm as $permName) {
    $perm = 0;
    if (!empty($use->posted[$permName]))
    {
      $perm = array_search($term->tid, $use->posted[$permName]);
      $perm = ($perm === FALSE ? 0 : 1);
    }

    $tp = termacl_perm_name($term->name, $permName);
    $use->return[$use->role->rid][$tp] = $perm;
    }

    '), $use);
    // Looping each term under $vid.
  }

  // Storing into db.
  array_walk($use->return, create_function('$item, $rid', 'user_role_change_permissions($rid, $item);return TRUE;'));
  drupal_set_message(t("The changes have been saved."));
}

/**
 * Implements.
 */
